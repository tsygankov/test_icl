from django import forms


class ExcelFileForm(forms.Form):
    file = forms.FileField(label='', 
                           widget=forms.FileInput(attrs={'class':'my_class'}))