from django.conf.urls import url

urlpatterns = [
    url(r'^$', 'excel_file.views.index', name='index'),
    url(r'^load/$', 'excel_file.views.load_data', name='load_data'),
    url(r'^get/$', 'excel_file.views.get_data', name='get_data')
]