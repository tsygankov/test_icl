# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('excel_file', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coordinate',
            name='x',
            field=models.FloatField(),
        ),
        migrations.AlterField(
            model_name='coordinate',
            name='y',
            field=models.FloatField(),
        ),
    ]
