import os
import xlrd

from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from excel_file.forms import ExcelFileForm
from excel_file.models import Coordinate


# Create your views here.
def index(request):
    file_form = ExcelFileForm()
    return render(request, 'excel_file/index.html', context={'file_form': file_form})

@csrf_exempt
def load_data(request):
    request.upload_handlers.pop(0)
    return _load_data(request)

@csrf_protect
def _load_data(request):
    if request.method == "POST":
        file_form = ExcelFileForm(request.POST, request.FILES)
        if file_form.is_valid():
            try:
                f = request.FILES['file']
                book = xlrd.open_workbook(f.temporary_file_path())
                sheet = book.sheet_by_index(0)
                coordinates = []
                #Cycle for checking of right data
                for row in range(10):
                    coordinates.append(Coordinate(x=sheet.cell_value(row, 0), y=sheet.cell_value(row, 1)))
                #Save into a database
                Coordinate.objects.all().delete()
                for coordinate in coordinates:
                    coordinate.save()
            except:
                return JsonResponse({'errors': ['Incorrect xls/xlsx']})
            return JsonResponse({'errors': [] })
        else:
            return JsonResponse({'errors': ['Form is invalid']})
    return JsonResponse({'errors': ['Method is not POST']})


def get_data(request):
    coordinates = Coordinate.objects.all()
    data = {'coordinates':[]}
    for coordinate in coordinates:
        data['coordinates'].append((coordinate.x, coordinate.y))
    return JsonResponse(data)

